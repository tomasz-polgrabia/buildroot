####################################################################
#
# SWITCHER
#
####################################################################
SWITCHER_VERSION = 1.0
SWITCHER_SITE = $(TOPDIR)/../switcher
SWITCHER_SITE_METHOD = local
SWITCHER_LICENSE = Proprietary
SWITCHER_DEPENDENCIES = 

define SWITCHER_BUILD_CMDS
$(MAKE) $(TARGET_CONFIGURE_OPTS) switcher -C $(@D)
endef
define SWITCHER_INSTALL_TARGET_CMDS
$(INSTALL) -D -m 0755 $(@D)/switcher $(TARGET_DIR)/usr/bin
endef
SWITCHER_LICENSE = GPL
$(eval $(generic-package))
