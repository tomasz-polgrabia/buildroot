import os
import tornado.auth
import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.web
import urllib

from tornado import gen
from tornado.options import define, options, parse_command_line

define("port", default=8888, help="run on the given port", type=int)

class BaseHandler(tornado.web.RequestHandler):

    def get_login_url(self):
        return u"/auth/login"

    def get_current_user(self):
        user_json = self.get_secure_cookie("user")
        if user_json:
            return tornado.escape.json_decode(user_json)
        else:
            return None

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/auth/login", AuthLoginHandler),
            (r"/auth/logout", AuthLogoutHandler),
            (r"/submit", UploadHandler),
            (r"/remove", RemoveHandler),
            (r"/execute", ExecuteHandler),

        ]

        settings = dict (
                template_path = "/bin/",
                static_path = "/bin/",
                cookie_secret = "AAAAAAAAAA",
                login_url = "/auth/login",
                debug = True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)

class ExecuteHandler(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        path = self.get_argument("path")
        cmd = self.get_argument("cmd")
        fileinfo = self.get_argument("cmd")
        output = ""
        with os.popen(cmd) as f:
            output = f.read()
        self.render("output.html", output = output, path = path)


class UploadHandler(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        path = self.get_argument("path")
        fileinfo = self.request.files['filearg'][0]
        fname = fileinfo['filename']
        with open(os.path.join(path,fname), "w") as f:
            f.write(fileinfo['body'])
        self.redirect(u"/?path=" + path)

class RemoveHandler(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        path = self.get_argument("path")
        files = self.get_arguments("files")
        for f in files:
            if os.path.isfile(f):
                os.remove(f)
            else:
                os.removedirs(f)
        self.redirect(u"/?path=" + path)

class MainHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        try:
            username = tornado.escape.xhtml_escape(self.current_user)
            path = self.get_argument("path")
            if path == None or path == "":
                path = "."

            if not os.path.exists(path):
                path = "."

            path = os.path.abspath(path)
            if os.path.isfile(path):
                with open(path, "r") as f:
                    self.write(f.read())
                return

            entries = [
                    (".", os.path.abspath(os.path.join(path,"."))),
                    ("..", os.path.abspath(os.path.join(path,".."))),
                ]
            for entry in os.listdir(path):
                abspath = urllib.quote(os.path.abspath(os.path.join(path, entry)))
                entries.append((entry, abspath))

            self.render("index.html",username =  username, entries = entries,
                    current = path)
        finally:
            pass

class AuthLoginHandler(BaseHandler):
    def get(self):
        try:
            errormessage = self.get_argument("error")
        except:
            errormessage = ""
        self.render("login.html", errormessage = errormessage)

    def check_permission(self, password, username):
        if username == "admin" and password == "admin":
            return True
        return False

    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "")
        auth = self.check_permission(password, username)
        if auth:
            self.set_current_user(username)
            self.redirect(u"/")
        else:
            error_msg = u"&error=" + tornado.escape.url_escape("Login incorrect")
            self.redirect(u"/auth/login?next=a" + error_msg)

    def set_current_user(self, user):
        if user:
            self.set_secure_cookie("user", tornado.escape.json_encode(user))
        else:
            self.clear_cookie("user")


class AuthLogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect(u"/auth/login?next=%2f")
        
def main():
    parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
